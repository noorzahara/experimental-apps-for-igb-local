package org.lorainelab.igb.ncbiprimer;

/**
 *
 * @author hiralv
 */
public interface NCBIPrimerBlastOpts {

    public RemotePrimerBlastNCBI.PrimerBlastOptions getOptions();
}
