# Experimental Apps for IGB 

These Apps are works-in-progress and have not yet been fully tested and released for wide use. 

However, they can serve as a useful guide for using the IGB API to build Apps for IGB.

# How to contribute

Please use the following fork-and-branch workflow:

* Create a bitbucket account and use it to fork the team repository at https://bitbucket.org/lorainelab/experimental-apps-for-igb.
* Make changes on topic branches specific for your new feature, bug fix or new App. Branch from the tip of the master branch. Please make one topic branch per new feature or bug fix as these are easier to merge and test.
* When your work is ready, please rebase your topic branch on the latest master and test. Also, please squash your commits into a single commit when feasible. Do your best to curate your commit history so that others can easily follow what you did, and why.
* When you are happy with the code and your commit history, a pull request from the topic branch on your fork to the master branch on the team repository. Use the Bitbucket UI to do this.  

When you submit a PR, the team repository admins will get an email letting them know. They will 
do their level best to get back to you quickly if they have questions. Otherwise, they will merge 
your changes. Depending on your Bitbucket configurations, you will probably get an email letting 
you know the PR has been merged. 

Note that any changes you make to your topic branch will be reflected in your
PR. If you make changes - e.g., re-basing on the master branch, squashing commits, etc - you
do not need to resubmit your PR. 

# Questions?

Please post questions and suggestions to: https://groups.google.com/forum/#!forum/igb-app-developers

# How to build and run all the Apps

* Clone the repository.
* Install Apache maven. 
* Change into the top-level directory.
* Enter: `mvn install`

This command builds compiles the Apps and copies them to a local App Repository in `obr/bundles`.

Once that is done, it uses `maven-bundle-plugin` to index all the Apps residing in `obr/bundles`. 
Indexing creates an OBR index file named `repository.xml` in the same location. 

Once this is done, you can install and run all the Apps in IGB by adding your local
folder `obr/bundles` as an App repository to IGB, using the IGB UI. When you do that, 
IGB reads the index file and add the newly available Apps to the IGB App Manager screen. 

To install and run the Apps in IGB, add `obr/bundles` as a local App Repository. To do this:

* Start IGB version 9.1.0 or higher
* Choose **Tools > Open App Manager**
* Click **Manage Repositories** button
* Click **Add** 
* Click **Choose local folder** and navigate to the obr/bundles folder

To confirm that the repository was added correctly, open the IGB App Manager:

* Select **Tools > Open App Manager**
* Confirm that you see the newly added Apps

**Note**: The App names displayed in IGB App Manager come from the <name> tag in each App's `pom.xml` file. 
During compilation, this value gets copied to the `Bundle-Name` header of the App jar file's `META-INF/MANFIEST.MF` file. 

# How to build and run just one App

Sometimes you may want to build, run, and install just one App. You can do that, too.

To build just one App:

* Change into the subdirectory for that App
* Enter: `mvn install`

This compiles just the one App to class files and a jar file (the App "bundle") in the App's `target` directory. Following compilation, also creates an OBR index file `repository.xml` for `target`. 
Since there is only one such App in `target`, the OBR index file lists just that one App.

To install and run the App in IGB, add the `target` directory as a new local App repository following the procedure above.

**Tip**: The maven-bundle-plugin is nothing but a convenient mvn-friendly wrapper around the `bnd`tool. To index any directory, you 
can type something like `mvn maven-bundle-plugin:index -DmavenRepository=target -DobrRepository=target` after installing `mvn`. Be sure 
to use the same `maven-bundle-plugin` version listed in the top-level `POM.xml` of the project. 

# How to install Apps from Bitbucket

The team repository located at https://bitbucket.org/lorainelab/experimental-apps-for-igb is configured to automatically build and copy the contents of `obr/bundles` to the Downloads folder. 
This occurs whenever a change is pushed to the repository. 
This means you can install and run the Apps without building the project yourself. 
To do this, add https://bitbucket.org/lorainelab/experimental-apps-for-igb/downloads/ as a new App repository to IGB and all the Apps will become avaiable for you to install and run.



